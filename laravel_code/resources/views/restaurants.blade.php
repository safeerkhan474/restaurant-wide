@extends('layouts.app')
@section('content')

@section('css')
<style type="text/css">
.strip_all_tour_list .col-lg-7.col-md-7.col-sm-7 {
    width: 74.333%;
}

</style>
@endsection
<section class="parallax-window" style="background-image:url('http://bsnscb.com/data/out/163/27009665-restaurant-wallpapers.jpg');">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1> Order from {{ count($listing) }} restaurants.</h1>
				<P>delivering to your door</P>
				
			</div>
		</div>
	</section>
	<!-- End section -->

<div id="position">
			<div class="container">
				<ul>
					<li><a href="#">Home</a>
					</li>
					<li><a href="#">Category</a>
					</li>
					<li>Page active</li>
				</ul>
			</div>
		</div>
		<!-- Position -->

		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map -->

		<div class="container margin_60">

			<div class="row">
				<aside class="col-lg-3 col-md-3">
					<p>
						<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a>
					</p>

					<div class="box_style_cat">
						
						<ul id="cat_nav">
							
							@foreach($cuisines as $row)
							<li><a href="#" data-id="{{ $row->id }}"><label><input type="checkbox"></label> {{ $row->name }} <span>(141)</span></a></li>
							@endforeach
						</ul>
					</div>

					<div id="filters_col">
						<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filters <i class="icon-plus-1 pull-right"></i></a>
						<div class="collapse" id="collapseFilters">
							<!-- <div class="filter_type">
								<h6>Price</h6>
								<input type="text" id="range" name="range" value="">
							</div> -->
							<div class="filter_type">
								<h6>Rating</h6>
								<ul>
									<li>
										<label>
											<input type="checkbox">
											<span class="rating">
											<i class="icon-smile voted"></i>
											<i class="icon-smile voted"></i>
											<i class="icon-smile voted"></i>
											<i class="icon-smile voted"></i>
											<i class="icon-smile voted"></i>
											</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
						<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i>
						</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
						<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i>
						</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
						<i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
						</span>
										</label>
									</li>
									<li>
										<label>
											<input type="checkbox"><span class="rating">
						<i class="icon-smile voted"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i><i class="icon-smile"></i>
						</span>
										</label>
									</li>
								</ul>
							</div>
							<div class="filter_type">
								<h6>District/Area</h6>
								<ul>
									<li>
										<label>
											<input type="checkbox">Paris Centre</label>
									</li>
									<li>
										<label>
											<input type="checkbox">La Defance</label>
									</li>
									<li>
										<label>
											<input type="checkbox">Latin Quarter</label>
									</li>
								</ul>
							</div>
						</div>
						<!--End collapse -->
					</div>
					<!--End filters col-->
					<div class="box_style_2">
						<i class="icon_set_1_icon-57"></i>
						<h4>Need <span>Help?</span></h4>
						<a href="tel://004542344599" class="phone">+45 423 445 99</a>
						<small>Monday to Friday 9.00am - 7.30pm</small>
					</div>
				</aside>
				<!--End aside -->
				<div class="col-lg-9 col-md-9">

					
					<!--/tools -->
					@if(isset($listing) && count($listing) > 0)
					@foreach($listing as $row)
					<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<!-- <div class="ribbon_3 popular"><span>Popular</span>
								</div> -->
								
								<div class="img_list">
									<a href="single_restaurant.html"><img src="http://www.thebusinesslogo.com/logo-design/restaurant-hotels/hotels-restaurants-logos-14.gif" alt="Image">
										
									</a>
								</div>
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="tour_list_desc">
									
									<a href="{{ url('restaurantDetail/'.Crypt::encrypt($row->restaurant_id)) }}"><h3>{{ $row->restaurant_name }}</h3></a>									<p>Fast food , burgr</p>
									
									<div class="rating"><i class="icon-smile voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile  voted"></i><i class="icon-smile"></i><small>(75)</small>
									</div>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 delivery">
                       			<p> delivers in <span class="minutes">50min</span></p>

                       			<span class="arrow_next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
							</div>
						
						</div>
					</div>
					@endforeach
					@else
						<div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
						<div class="row" style="text-align:center;">
							<h2>No Result Found</h2>
						</div>
						</div>
					@endif
					<!--End strip -->

				





				
					<!-- end pagination-->

				</div>
				<!-- End col lg-9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->

@section('js')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVcu5cF2q7EulpPdL5RvlgrXPTwWaeVWc&callback=initMap"
  type="text/javascript"></script>
  <script src="{{ asset('assets/js/map.js') }}"></script>

@endsection

@endsection