@extends('layouts.app')
@section('content')

@section('css')

@endsection
<!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a></li>
                <li><a href="#0">Category</a></li>
                <li>Page active</li>
            </ul>
        </div>
    </div><!-- Position -->

<!-- Content ================================================== -->
<div class=" container margin_60">
<div id="container_pin">
		<div class="row">
	
			<div class="col-md-9">
				<div class="box_style_2">
					<h2 class="inner">Payment methods</h2>
				
					
					<div class="payment_select nomargin">
						<label class=""><div class="iradio_square-grey" style="position: relative;"><input value="" name="payment_method" class="icheck" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Pay with cash</label>
					</div>
				</div><!-- End box_style_1 -->

				<div class="box_style_2">
					<div class="panel">
            <div class="panel-heading">
            <h3 class="panel-title">My Contact Details</h3>
        </div>
        <div class="panel-body">
            
    <input id="shop_checkout_type_orderMetaData_customer_email" name="shop_checkout_type[orderMetaData][customer][email]" required="required" class="hide" readonly="readonly" value="miang@gmail.com" type="hidden">
    <input id="shop_checkout_type_orderMetaData_customer_firstName" name="shop_checkout_type[orderMetaData][customer][firstName]" required="required" class="hide" readonly="readonly" value="Mian" type="hidden">
    <input id="shop_checkout_type_orderMetaData_customer_lastName" name="shop_checkout_type[orderMetaData][customer][lastName]" required="required" class="hide" readonly="readonly" value="G" type="hidden">

<dl class="checkout-customer-details">
    <dt>Full Name</dt>
    <dd>Mian G</dd>

    <dt>E-mail</dt>
    <dd>
        miang@gmail.com
            
    </dd>

                <input id="shop_checkout_type_orderMetaData_customer_mobileCountryCode" name="shop_checkout_type[orderMetaData][customer][mobileCountryCode]" required="required" readonly="readonly" class="hide" value="92" type="hidden">
            <input id="shop_checkout_type_orderMetaData_customer_mobileNumber" name="shop_checkout_type[orderMetaData][customer][mobileNumber]" required="required" data-mask="" class="hide" readonly="readonly" value="3014550134" type="hidden">
        <dt>Mobile</dt>
        <dd>
            +92 3014550134
                
        </dd>
    
    
    <dt></dt>
  
</dl>

        </div>
    </div>
				</div><!-- End box_style_1 -->
			</div><!-- End col-md-6 -->
            
			<div class="col-md-3">
				<div class="pin-wrapper" style="height: 560px;"><div id="cart_box" style="width: 263px;">
					<h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
					<table class="table table_summary">
					<tbody>
					<tr>
						<td>
							<a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>1x</strong> Enchiladas
						</td>
						<td>
							<strong class="pull-right">$11</strong>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>2x</strong> Burrito
						</td>
						<td>
							<strong class="pull-right">$14</strong>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>1x</strong> Chicken
						</td>
						<td>
							<strong class="pull-right">$20</strong>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>2x</strong> Corona Beer
						</td>
						<td>
							<strong class="pull-right">$9</strong>
						</td>
					</tr>
					<tr>
						<td>
							<a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>2x</strong> Cheese Cake
						</td>
						<td>
							<strong class="pull-right">$12</strong>
						</td>
					</tr>
					</tbody>
					</table>
					<hr>
					<div class="row" id="options_2">
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
							<label><div class="iradio_square-grey checked" style="position: relative;"><input value="" checked="" name="option_2" class="icheck" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Delivery</label>
						</div>
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
							<label class=""><div class="iradio_square-grey" style="position: relative;"><input value="" name="option_2" class="icheck" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>Take Away</label>
						</div>
					</div><!-- Edn options 2 -->
					<hr>
					<table class="table table_summary">
					<tbody>
					<tr>
						<td>
							 Subtotal <span class="pull-right">$56</span>
						</td>
					</tr>
					<tr>
						<td>
							 Delivery fee <span class="pull-right">$10</span>
						</td>
					</tr>
					<tr>
						<td class="total">
							 TOTAL <span class="pull-right">$66</span>
						</td>
					</tr>
					</tbody>
					</table>
					<hr>
					<a class="btn_full" href="cart_3.html">Confirm your order</a>
				</div></div><!-- End cart_box -->
			</div><!-- End col-md-3 -->
            
		</div><!-- End row -->
	</div>
</div><!-- End container -->
<!-- End Content =============================================== -->




@section('js')


@endsection

@endsection