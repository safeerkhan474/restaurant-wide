
@extends('layouts.app')
@section('content')

@section('css')
@endsection

<section class="header-video">
            <div id="hero_video">
                <div class="intro_title">
                    <img src="assets/img/white_logo.png">
                    <h3 class="animated fadeInDown">Pakistan largest Restaurants site. </h3>
                    <p class="animated fadeInDown">Know better. Book better. Go better.</p>
                    <div id="search_bar_container" style="z-index:9999">
                <div class="container">
                    <div class="cover">

                    <div class="search_bar">
                        <span class="nav-facade-active" id="nav-search-in">
                                <span id="nav-search-in-content" style="">Select City</span>
                        <span class="nav-down-arrow nav-sprite"></span>
                        <select title="Search in" class="searchSelect" id="searchDropdownBox" name="tours_category">
                        <option></option> 
                        @if(isset($cities) && count($cities) > 0)
                        @foreach($cities as $row)
                        <option value="{{ $row->city_id }}">{{ $row->city_name }}</option>
                        @endforeach()
                        @else
                        <option>NO City Found</option>
                        @endif          
                        </select>
                        </span>
                        <form method="post" action="{{ url('get_slug') }}">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <div class="nav-searchfield-outer">
                            <input type="text" class="areas_listing" data-id=""  autocomplete="off" name="areaname" placeholder="Type your search terms ...." id="twotabsearchtextbox">
                            <div class="spin"></div>
                            <div class="spin_two"></div>
                            <button type="submit" class="animated fadeInUp button_intro"><i class="fa fa-compass" aria-hidden="true"></i></button>
                        </div>
                        </form>
                    </div>
                    
                    </div>
                    <!-- End search bar-->
                </div>
            </div>
                </div>
            </div>

            <!-- /search_bar-->
            <img src="" alt="Image" class="header-video--media" data-video-src="" data-teaser-source="assets/video/11916130" data-provider="Youtube" data-video-width="854" data-video-height="330">
        </section>
        <!-- End Header video -->


  
        <div class="container margin_60">

            <div class="main_title">
                <h2>Some <span>good</span> reasons</h2>
                <p>
                    Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
                </p>
            </div>

            <div class="row">

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3>SEARCH</h3>
                        <p>
                           Explore restaurants that deliver to your doorstep.All our restaurants deliver everywhere 
                        </p>
                       
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3>PICK A RESTAURANT</h3>
                        <p>
                            Browse menus and build your order in seconds . Choose all the items you want from the menu.
                        </p>
                        
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-57"></i>
                        <h3>ORDER FOOD</h3>
                        <p>
                            We won’t put you on hold, or make you listen to annoying offers. You will want to call us!
                        </p>
                        
                    </div>
                </div>

            </div>
            <!--End row -->

            <hr>

            <div class="row">
                <div class="col-md-8 col-sm-6 hidden-xs">
                    <img src="assets/img/laptop.png" alt="Laptop" class="img-responsive laptop">
                </div>
                <div class="col-md-4 col-sm-6">
                    <h3><span>Get started</span> with CityTours</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                    </p>
                    <ul class="list_order">
                        <li><span>1</span>Select your preferred tours</li>
                        <li><span>2</span>Purchase tickets and options</li>
                        <li><span>3</span>Pick them directly from your office</li>
                    </ul>
                    <a href="all_tour_list.html" class="btn_1">Start now</a>
                </div>
            </div>
            <!-- End row -->

        </div>
        <!-- End container -->


        
@section('js')


@endsection

@endsection