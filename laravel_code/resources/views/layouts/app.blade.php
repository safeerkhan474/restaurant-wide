<!DOCTYPE html>
<html lang="en" data-textdirection="LTR" class="loading">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets.">
    <meta name="author" content="Ansonika">
    <title>Restaurants Wide</title>
    
    <!-- Favicons-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
    
    <script src="https://use.fontawesome.com/e3180e35d3.js"></script>

    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">
    <link href="{{ asset('assets/css/base.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/skins/square/grey.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/jquery.switch.css') }}" rel="stylesheet">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css" rel="stylesheet">

    @section('css')
    @show
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
  </head>

<body>
      <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

 <div class="layer"></div>
    <!-- Mobile menu overlay mask -->
 <!-- Header================================================== -->
    <header>
      
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                        <h1><a href="{{ url('/') }}" title="Restaurant Wide">Restaurant Wide</a></h1>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="img/logo_sticky.png" width="160" height="34" alt="City tours" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                      
                    </div><!-- End main-menu -->
                    <ul id="top_tools">
                         <li>
                                <div class="dropdown dropdown-access">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">Sign in</a>
                                    <div class="dropdown-menu">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_facebook">
                                                    <i class="icon-facebook"></i>Facebook </a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_paypal">
                                                    <i class="icon-paypal"></i>Paypal </a>
                                            </div>
                                        </div>
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">or</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputUsernameEmail" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                        </div>
                                        <a id="forgot_pw" href="#">Forgot password?</a>
                                        <input type="submit" name="Sign_in" value="Sign in" id="Sign_in" class="button_drop">
                                        
                                    </div>
                                </div><!-- End Dropdown access -->
                            </li>
                            <li><a href="{{ url('signup') }}" >Sign Up</a></li>
                    </ul>
                </nav>
            </div>
        </div><!-- container -->
    </header><!-- End Header -->
    
    <main>
 @yield('content')
    </main>
<footer class="revealed">
        <div class="container footer_area">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h2>Restaurant wide</h2>
                    <p>Then Restaurant wide is the right place for you! Restaurant wide, offers you a long and detailed list of the best restaurants near you. Whether it is a delicious Pizza, Burger, Japanese or any kind of Fast Food you are craving, foodpanda has an extensive roaster of  restaurants; further, if you are in the mood for Indian, Pakistani, or Afghan cuisines, there are plenty of restaurants available for you. Restaurant wide is available in a total of 9 cities in Pakistan, among which are the top popular four cities: Islamabad, Lahore, Rawalpindi, and Karachi.</p>
                </div>
              
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                        
                        </ul>
                        <p>© Copyright 2017 Restaurant wide  </p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->

    <div id="toTop"></div><!-- Back to top button -->
    
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon_set_1_icon-78"></i>
            </button>
        </form>
    </div><!-- End Search Menu -->
@section('js')
   
   
<!-- Jquery -->
<script src="{{ asset('assets/js/jquery-2.2.4.min.js') }} "></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{ asset('assets/js/common_scripts_min.js') }}"></script>
<script src="{{ asset('assets/js/functions.js') }}"></script>
<script src="{{ asset('assets/js/modernizr.js') }}"></script>  
<script src="{{ asset('assets/js/video_header.js') }}"></script>
<script src="{{ asset('assets/js/icheck.js') }}"></script>

    <script>
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });
    </script>

    <script src="{{ asset('assets/js/tabs.js') }}"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>
    <script>
        $('.wishlist_close_admin').on('click', function (c) {
            $(this).parent().parent().parent().fadeOut('slow', function (c) {});
        });
    </script>
<script type="text/javascript">
$(document).ready(function() {
   HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: false
    });    
});

 
$('#searchDropdownBox').on('change', function() {
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  var id = this.value;

  $("#nav-search-in-content").text($(this).find("option:selected").text());

  $.ajax({
    type: "POST",
    url: "{{ url('get_areas') }}",
    data: {id:id,_token:CSRF_TOKEN},
    beforeSend: function(){
        $(".nav-searchfield-outer .spin").html('<span><i class="fa fa-refresh fa-spin"></i></span>');
    },
    success: function(data){
      if(data.success == true)
      {
            $(".nav-searchfield-outer .spin").html('');
            $('#twotabsearchtextbox').attr('data-id', id);

        }
      }
    
});
});

    $( ".areas_listing" ).autocomplete({
          
          source: function(request, response) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
              $.ajax({
                url: "{{ url('get_selected_areas') }}",
                data: { id: $("#twotabsearchtextbox").data('id'), name: $("#twotabsearchtextbox").val(),_token:CSRF_TOKEN},
                dataType: "json",
                type: "POST",
                 beforeSend: function(){
                    $(".nav-searchfield-outer .spin_two").html('<span><i class="fa fa-refresh fa-spin"></i></span>');
                },
                success: function(data){
                    $(".nav-searchfield-outer .spin_two").html('');
                    var resp = $.map(data,function(obj){
                      return obj.area_name;
                    }); 
                response(resp);  
            }
        });
      },
      minLength: 2,

      });

</script>
    <!-- Page JS -->
    @section('js')
    @show
    </body>
</html>