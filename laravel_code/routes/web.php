<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Home@index');
Route::post('/get_areas', 'Home@get_areas');
Route::post('/get_selected_areas', 'Home@get_selected_areas');
Route::post('/get_slug', 'Home@get_slug');
Route::get('/restaurants/{area_slug}', 'Home@restaurants');
Route::get('/restaurantDetail/{restaurant_id}', 'Home@restaurantDetail');

Route::get('/cart', 'Cart@index');
Route::get('/checkout', 'Cart@checkout');


Route::get('/signup', 'Customer@index');
