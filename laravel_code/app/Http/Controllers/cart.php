<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Cities;
use App\Cuisines;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class Cart extends BaseController
{
   public function index(){
   		$data = [];
   		return view('cart', $data);
   }

    public function checkout(){
         $data = [];
         return view('checkout', $data);
   }

 

}
