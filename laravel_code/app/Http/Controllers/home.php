<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Cities;
use App\Cuisines;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class Home extends BaseController
{
   public function index(){
   		$data = [];
   		$data['cities'] = Cities::all();
   		return view('home', $data);
   }

   public function get_areas(Request $request){
   	$count = DB::table('areas')->where('city_id', $request->id)->count();
   	
      if($count){
   		return response()->json(array("success" => true, "areas" => $count));
   	}else{
   		return response()->json(array("success" => false, "areas" => "No results"));
   	}
   }

   public function get_selected_areas(Request $request)
   {
      if($request->id && $request->id != '')
      {
         $data = DB::table('areas')->where('city_id', $request->id)->where('area_name', 'like', '%' . $request->name . '%')->get();
      }else{
         $data = DB::table('areas')->where('area_name', 'like', '%' . $request->name . '%')->get();
      }
      
      return response()->json($data);
   }

   public function get_slug(Request $request)
   {
      $data = DB::table('areas')->where('area_name', $request->areaname)->where('status', 1)->take(1)->first();
      return redirect('restaurants/'.$data->area_slug);
   }

   public function restaurants(Request  $request)
   {
      $data = [];
      $data['listing'] = DB::table('restaurants as r')
            ->join('delivery_areas as d', 'r.restaurant_id', '=', 'd.restaurant_id')
            ->join('areas as a', 'd.area_id', '=', 'a.area_id')
            ->where('a.area_slug', '=', $request->segment(2))
            ->get();
      $data['cuisines'] = Cuisines::get();
      return view('restaurants', $data);
   }

   public function restaurantDetail(Request  $request)
   {

      $data = [];
      $data['restaurant'] = DB::table('restaurants')->where('restaurant_id', Crypt::decrypt($request->segment(2)))->first();
      $data['categories'] = DB::table('categories')->where('restaurant_id', Crypt::decrypt($request->segment(2)))->get();

      $categories = DB::table('categories')->where('restaurant_id', Crypt::decrypt($request->segment(2)))->get();
      foreach ($categories as $row) {
             $row->menuitems_list = DB::table('menu_items as m')
                                ->join('categories as c', 'm.category_id', '=', 'c.category_id')
                                ->where('m.category_id', '=', $row->category_id)->get();

               foreach ($row->menuitems_list as $menu) {
                  $menu->prices = DB::table('menuprice')->where('menu_id', '=', $menu->menu_item_id)->get();
               }
          }
      $data['menuitems'] = $categories;

      //print_r($data['menuitems']);exit;
      return view('restaurantDetail', $data);
   }
}
