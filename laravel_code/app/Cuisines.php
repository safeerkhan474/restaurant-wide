<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuisines extends Model{
    protected $table = 'cuisines';
    protected $primaryKey  = 'id';
}
